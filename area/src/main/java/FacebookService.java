import com.restfb.*;
import com.restfb.exception.FacebookException;
import com.restfb.types.FacebookType;
import com.restfb.types.GraphResponse;
import com.restfb.types.Post;
import com.restfb.types.User;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;

public class FacebookService {
    public static FacebookClient fbClient;
    public String apiKey = "176702006313352";
    public String apiSecret = "4372ad1f00f7d43ac1da564f35f40029";
    // user token pour moi. Pour test il faudra generer un access token pour ton utilisateur dans FB developer
    public String accessToken = "EAACEdEose0cBACtncxsZBKOF5L9m8XbMMjmBT6Co2GIRe627Yx2s1KYCihTcUyqH9SXUxl9gut6rY70yZCYF1zTyrXklYOfwo9CQxQHJibsKLXUMb35DOZBowH3JE9t9EQpCcsdfT6ELgm3b0WNutAZBMCA0HvWQ3VN1Q7hoZCXITrStuBwQpzA9f8icowddDwXyUhULi5AZDZD";
    public User me;

    public FacebookService() {
        try {
            fbClient = new DefaultFacebookClient(accessToken, Version.LATEST);
            FacebookClient.AccessToken token = fbClient.obtainAppAccessToken(apiKey, apiSecret);
            me = fbClient.fetchObject("me", User.class);
        } catch (FacebookException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    public void postMsg(String msg) {
        try {
            GraphResponse publishMsg = fbClient.publish("me/feed", GraphResponse.class, Parameter.with("message", msg));
            System.out.println("Successfully updated status to [" + msg + "].");
        } catch (FacebookException e) {
            e.printStackTrace();
        }
    }

    private static byte[] fetchBytesFromImage(String imageFile, String format) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte fileContent[] = null;
        try{
            BufferedImage image = ImageIO.read(new File(imageFile));
            ImageIO.write(image, format, baos);
            baos.flush();
            fileContent = baos.toByteArray();
        }catch (Exception e){
            e.printStackTrace();
        }
        return  fileContent;
    }

    @SuppressWarnings("unused")
    public void postPic(String path, String format, String message) {
        try {
            @SuppressWarnings("deprecation")
            byte[] imageAsBytes = fetchBytesFromImage(path, format);
            GraphResponse graphResponse = fbClient.publish("me/photos" , GraphResponse.class,
                    BinaryAttachment.with(path, imageAsBytes, path),
                    Parameter.with("message", message));
            System.out.println("Successfully uploaded photo with Id " + graphResponse.getId());
        } catch (FacebookException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    public void postVid(String path, String format, String message) {
        try {
            @SuppressWarnings("deprecation")
            byte[] imageAsBytes = fetchBytesFromImage(path, format);
            GraphResponse publishVideoResponse = fbClient.publish("me/videos" , GraphResponse.class,
                    BinaryAttachment.with(path, imageAsBytes, path),
                    Parameter.with("message", message));
            System.out.println("Successfully uploaded video with ID " + publishVideoResponse.getId());
        } catch (FacebookException e) {
            e.printStackTrace();
        }
    }
}
