import twitter4j.*;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;

public class TwitterService {
    private static String _apiKey = "IfhYEmLMUkfZhyz9p3q7fr6ob";
    private static String _apiSecret = "AqvHWRpXRpQFZ8yp4VCZ9SMrwuvLqSlBY0eTnHThgrqZ1pqDoR";
    private static int _userId;
    private static String _token;
    private static String _tokenSecret;
    public Twitter twitter;

    public TwitterService() throws TwitterException, IOException {
        twitter = TwitterFactory.getSingleton();
        twitter.setOAuthConsumer(_apiKey, _apiSecret);
        Scanner sc = new Scanner(System.in);
        RequestToken requestToken = twitter.getOAuthRequestToken();
        AccessToken accessToken = null;
        while (null == accessToken) {
            System.out.println("Open the following URL and grant access to your account:");
            System.out.println(requestToken.getAuthorizationURL());
            System.out.println("Paste the code gotten in the browser (at the end of the URL):  ");
            String pin = sc.nextLine();  //SCAN VERIFIER CODE
            try {
                if (pin.length() > 0) {
                    accessToken = twitter.getOAuthAccessToken(requestToken, pin);
                } else {
                    accessToken = twitter.getOAuthAccessToken();
                }
            } catch (TwitterException te) {
                if (401 == te.getStatusCode()) {
                    System.out.println("Unable to get the access token.");
                } else {
                    te.printStackTrace();
                }
            }
        }
        storeAccessToken((int) twitter.verifyCredentials().getId(), accessToken);
    }

    private static void storeAccessToken(int userId, AccessToken accessToken) {
        _userId = userId;
        _token = accessToken.getToken();
        _tokenSecret = accessToken.getTokenSecret();
    }

    public void PostTweet(String latestStatus) {
        try {
            Status status = twitter.updateStatus(latestStatus);
            System.out.println("Successfully updated status to [" + status.getText() + "].");
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    public void GetTimeline() {
        List<Status> statuses = null;
        try {
            statuses = twitter.getHomeTimeline();
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        System.out.println("Showing home timeline: ");
        for (Status status : statuses) {
            System.out.println(status);
        }
    }

    // To send a message, the recipient must follow @AreaProut
    public void SendDM(String recipient, String body) {
        try {
            DirectMessage message = twitter.sendDirectMessage(recipient, body);
            System.out.println("Sent: " + message.getText() + " to @" + message.getRecipientScreenName());
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    public void SearchTweet(String searchString) {
        Query query = new Query(searchString);
        QueryResult result = null;
        try {
            result = twitter.search(query);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        for (Status status : result.getTweets()) {
            System.out.println("@" + status.getUser().getScreenName() + ":" + status.getText());
        }
    }
}
